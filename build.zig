const std = @import("std");
const builtin = @import("builtin");
const rp2040 = @import("rp2040");

pub fn build(b: *std.Build) void {
    const uf2_dep = b.dependency("uf2", .{});

    const rp2040_dep = b.dependency("rp2040", .{});
    const rp2040_flasher = rp2040_dep.artifact("rp2040-flash");

    const mdf_dep = b.dependency("mdf", .{});
    const exe = rp2040.addPiPicoExecutable(b, .{
        .name = "picolun",
        .source_file = .{ .path = "src/main.zig" },
        .optimize = b.standardOptimizeOption(.{}),
    });
    exe.addAppDependency("drivers", mdf_dep.module("drivers"), .{ .depend_on_microzig = true });
    exe.installArtifact(b);

    const elf2uf2_run = b.addRunArtifact(uf2_dep.artifact("elf2uf2"));

    // family id
    elf2uf2_run.addArgs(&.{ "--family-id", "RP2040" });

    // elf file
    elf2uf2_run.addArg("--elf-path");
    elf2uf2_run.addArtifactArg(exe.inner);

    // output file
    elf2uf2_run.addArg("--output-path");
    const uf2_file = elf2uf2_run.addPrefixedOutputFileArg("", "test.uf2");

    const install_uf2 = b.addInstallFile(uf2_file, "picolun.uf2");
    install_uf2.dir = .{ .custom = "firmware" };

    b.getInstallStep().dependOn(&install_uf2.step);

    const run_flash = b.addRunArtifact(rp2040_flasher);
    run_flash.addArg("--wait");
    run_flash.addFileSourceArg(uf2_file);

    const flash_step = b.step("flash", "flashes the system");
    flash_step.dependOn(&run_flash.step);
}
