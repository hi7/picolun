const std = @import("std");
const microzig = @import("microzig");

const rp2040 = microzig.hal;
const uf2 = @import("uf2");
const time = rp2040.time;
const gpio = rp2040.gpio;
const clocks = rp2040.clocks;
const peripherals = microzig.chip.peripherals;
const mdf = microzig.drivers;
const st77xx = microzig.drivers.driver.display.st77xx;

const BUF_LEN = 0x100;
const spi = rp2040.spi.num(0);
// ST7735 TFT controller
// SCK: Pin04 GP2
// SDA: Pin05 GP3
// A0:  Pin01 GP0
// RSX: Pin36 3.3V out
// CSX: Pin02 GP1

const pin_config = rp2040.pins.GlobalConfiguration{
    .GPIO25 = .{
        .name = "led",
        .direction = .out,
    },
};

pub fn main() !void {
    //var channel = mdf.base.DatagramDevice.init_receiver_only();
    //defer channel.deinit();

    const pins = pin_config.apply();

    pins.led.toggle();
    time.sleep_ms(250);
    pins.led.toggle();
    time.sleep_ms(250);
    pins.led.toggle();
    time.sleep_ms(250);

    spi.apply(.{
        .clock_config = rp2040.clock_config,
    });
    var out_buf: [BUF_LEN]u8 = .{ 0xAA, 0xBB, 0xCC, 0xDD } ** (BUF_LEN / 4);
    var in_buf: [BUF_LEN]u8 = undefined;

    while (true) {
        _ = spi.transceive(&out_buf, &in_buf);
        time.sleep_ms(1 * 1000);
    }
}
